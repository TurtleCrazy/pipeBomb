#include <unistd.h>
#include <map>
#include "messages.h"

membuf::membuf( size_t size){// may use size max
	m_buffer.reserve(size);
	reset();
}

void membuf::reset(){
	m_cursor=sizeof m_cursor;
	m_buffer.resize(m_cursor);
}

membuf::~membuf(){
}

void membuf::write(const char* c,size_t s){
	size_t newsize(m_buffer.size()+s);
	m_buffer.resize(newsize);
	while(m_cursor<newsize){
		m_buffer[m_cursor++]=*(c++);
	}
	m_buffer[0]= (m_cursor>> 24);
	m_buffer[1]= (m_cursor>> 16);
	m_buffer[2]= (m_cursor>> 8);
	m_buffer[3]= (m_cursor);

}
void membuf::read(char*c ,size_t s){
	size_t nc(m_cursor+s);
	if(nc>m_buffer.size())
		return;
	while(m_cursor<nc){
		*(c++)=m_buffer[m_cursor++];
	}
}

const char* membuf::buffer() const{
	return m_buffer.data();
}

size_t membuf::size() const {
	return m_buffer.size();
}

void membuf::set(const char* i0, const char* i1) {
	reset();
	m_buffer.insert(m_buffer.begin()+sizeof m_cursor,i0,i1);
}


typedef pipeMessage* (*Factory)(membuf&);//avoid construct of first use issue 
static std::map<int,Factory> pipeMessages;
void registerMessage(int id, pipeMessage* (*Factory)(membuf&)){
	pipeMessages[id]=Factory;
}
nullMessage::nullMessage():pipeMessage(){
};
nullMessage::~nullMessage(){
};

int nullMessage::id() const { return 0;}
void nullMessage::serialize(membuf& out) const{
}
pipeMessage* nullMessage::create(membuf& in) { 
	return new nullMessage();
};


pipeMessage* pipeMessage::deserialize(membuf& istr) {
	int classId(0);
	istr.read(reinterpret_cast<char*>(&classId),sizeof (int));
	if(pipeMessages.count(classId)==0) {
		return new nullMessage;
	}
	return pipeMessages[classId](istr);
}

pipeMessage::pipeMessage(){
}

pipeMessage::~pipeMessage(){
}

testMessage::testMessage(double d):pipeMessage(),m_data(d){
};
testMessage::~testMessage(){
};
pipeMessage* testMessage::create(membuf& in) { 
	double d;
	in.read(reinterpret_cast<char*>(&d), sizeof d);
	return new testMessage(d);
};

int testMessage::id() const { return m_id;};
void testMessage::serialize(membuf& out) const{
	out.write(reinterpret_cast<const char*>(&m_id),sizeof (int));
	out.write(reinterpret_cast<const char*>(&m_data),
			sizeof m_data);
}

double testMessage::data() const{
	return m_data;
}

lastMessage::lastMessage():pipeMessage(){
};
lastMessage::~lastMessage(){
};
pipeMessage* lastMessage::create(membuf& in) { 
	return new lastMessage();
};

int lastMessage::id() const { return m_id;};
void lastMessage::serialize(membuf& out) const{
	out.write(reinterpret_cast<const char*>(&m_id),sizeof (int));
}

int testMessage::m_id=1;
int lastMessage::m_id=2;

