#include <stdint.h>
#include <vector>

class membuf;

class membuf //to avoid streambuf from boost
{
	public:
		membuf( size_t size=64 );
		~membuf();
		void write(const char* c,size_t s);
		void read(char*c ,size_t s);
		const char* buffer() const;
		void set(const char* , const char*);
		size_t size() const;
		void reset();
	private:
		std::vector<char> m_buffer;
		uint32_t m_cursor;
};
class pipeMessage{
	public:

		pipeMessage();
		virtual ~pipeMessage();

		virtual void serialize(membuf&) const=0;
		static	pipeMessage* deserialize(membuf& istr);
		virtual int id()const =0;
};

void registerMessage(int id, pipeMessage* (*Factory)(membuf&));
class nullMessage:public pipeMessage{
	public:
		nullMessage();
		~nullMessage();
		static	pipeMessage* create(membuf& in);
		int id() const;
		void serialize(membuf& out) const;
};


class lastMessage:public pipeMessage{
	public:
		lastMessage();
		~lastMessage();
		static	pipeMessage* create(membuf& in);
		int id() const;
		void serialize(membuf& out) const;
	private:
		static int m_id; // may be automatically calculated on registering
};

class testMessage:public pipeMessage{
	public:
		testMessage(double d);
		~testMessage();
		static	pipeMessage* create(membuf& in);
		int id() const;
		void serialize(membuf& out) const;
		double data() const;
	private:
		double m_data;
		static int m_id; // may be automatically calculated on registering
};

